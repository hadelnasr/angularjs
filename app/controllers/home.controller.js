app.controller("homeController", function($scope, homeService){
    $scope.car = {};
    $scope.cars = [];
    
    homeService.loadCars($scope.cars);
    
    $scope.save = function () {
        $scope.car = homeService.save($scope.cars, $scope.car);
    };
    
    $scope.update = function(newCar){
        $scope.car = homeService.update($scope.cars, $scope.selectedIndex, newCar);
        $scope.editMode = false;
    };
    
    $scope.edit = function(car, index){
        $scope.editMode = true;
        $scope.car = angular.copy(car);
        $scope.selectedIndex = index;
    };
    
    $scope.delete = function(index){
        $scope.cars.splice(index, 1);
    };  
    
    $scope.reset = function(){
        $scope.car = homeService.reset();
        $scope.editMode = false;
    };
});