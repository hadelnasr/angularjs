var homeService = app.service('homeService', function () {
    var that = this;
    
    this.loadCars = function(container){
        var object = {
            name: "verna",
            model: "2006",
            color: "red"
        };
        container.push(object);

        object = {
            name: "lancer",
            model: "2010",
            color: "blue"
        };
        container.push(object);

        object = {
            name: "mercede",
            model: "1995",
            color: "green"
        };
        return container.push(object);
    };
    
    this.save = function(container, object){
        container.push(object);
        return that.reset();
    };
    
    this.update = function(container, selectedIndex, newCar){
        container.splice(selectedIndex, 1, newCar);
        return that.reset();
    };
    
    this.reset = function(){
        return {};
    };
});