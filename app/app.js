var app = angular.module("angularApp", ["ngRoute"]);
app.config(function ($routeProvider) {
    $routeProvider
    .when("/form", {
        templateUrl: "form.html"
    })
    .when("/listing", {
        templateUrl: "listing.html"
    });
});